# Client Errors
client_error = 99
# Server Error
server_error = 100
# Login Codes
already_logged_in = 101
no_user = 102
wrong_pwd = 103
empty_uid = 104
empty_pwd = 105
auth_failed = 106

# Token & Permission Codes
wrong_token = 111
expired_token = 112
permission_denied = 113

# Cluster Codes
cluster_not_found = 121
cluster_already_exists = 122
incomplete_cluster_info = 123
cluster_name_blank = 124

# User management codes
username_already_exists = 131
user_exists = 132
user_not_found = 133
incomplete_user_information = 134
incorrect_primaryRole = 135
user_already_deactivated = 136
cannot_modify_password = 137
call_deactivate_user = 138
password_not_valid = 139
uid_format_invalid = 339
primary_role_invalid = 340
ldap_user_addition_failed = 341
invalid_user_fields = 342
ldap_user_deletion_failed = 343

# Node management codes
node_already_exists = 141
incomplete_node_information = 142
node_unavailable = 143
node_not_found = 144
kubernetes_error = 145
invalid_node_data = 146
incomplete_provision_information = 147
invalid_provision_information = 148
invalid_node_type = 149
node_assign_failed = 150
invalid_master_node = 211
master_not_provisioned = 212
node_already_provisioned = 213
node_already_deactivated = 214
call_deactivate_node = 215
node_already_assigned = 216
node_not_provisioned = 217
create_cluster_first = 218
node_provision_failed = 219

# Project Management Codes
incomplete_project_info = 151
project_not_found = 152
project_build_failed = 153
deployment_creation_failed = 154
project_deployment_failed = 155
project_undeployment_failed = 156
invalid_build_version = 157
components_specified_incorrectly = 158
service_creation_failed = 159
namespace_creation_failed = 160
branch_not_specified = 161
currently_not_deployed = 162
port_patching_attempted = 163
job_creation_failed = 164
cronjob_creation_failed = 165
invalid_cron_schedule = 166
invalid_job_type = 167
invalid_job_commands = 168
invalid_project_name = 169
docker_secret_creation_failed = 170
service_account_creation_failed = 601
role_binding_creation_failed = 602

# Project management codes
incomplete_project_information = 171
invalid_project_format = 172
invalid_project_field_format = 173
project_already_exists = 174
projectname_already_exists = 175
project_not_created = 176
project_currently_deployed = 177
project_deactivation_failed = 178
component_already_exists = 179
activate_project_first = 180
call_deactivate_method = 181
invalid_owner_information = 182
invalid_developer_information = 183
developer_not_found = 184
internal_config_error = 185
unknown_component_key = 186
invalid_component_format = 187
incorrect_component_fields = 188
project_setup_failed = 189
repo_clone_failed = 190
incorrect_pipelines_information = 191

# Bitbucket transaction management codes
project_creation_failed = 192
repo_creation_failed = 193
skeleton_repo_creation_failed = 194
project_push_failed = 195
invalid_code_repo_manager = 196
invalid_build_project_input = 197
project_corrupted_exception = 198
build_manager_exception = 199
code_repo_manager_exception = 501

# General Xpresso Errors
blank_field_error = 201
missing_field_error = 202
invalid_value_error = 203
file_not_found = 204
json_load_error = 205
pv_upper_limit_error = 206
unsupported_deployment_flavor = 207
invalid_field_type = 208

# Database Operation Codes
unsuccessful_connection = 221
unsuccessful_operation = 222
primary_key_violation = 223

# Kubeflow Error Codes
declarative_json_incorrect = 231
reference_not_found = 232
pipeline_not_found = 233
kubeflow_dashboard_port_fetching_failed = 234
pipeline_upload_failed = 235

# API Gateway
gateway_connection_error = 241
gateway_duplicate_error = 242

# deployment environments
invalid_environment_error = 251
no_clusters_present_error = 252
incorrect_deployment_error = 253

# Serialization
serialization_failed = 261
deserialization_failed = 262
json_key_error = 263

# Bundle Exceptions
bundle_failed = 271
bundle_unsupported = 272

# Pachyderm Repo Management error codes
pachyderm_repo_not_provided = 301
dataset_info_error = 302
dataset_path_invalid = 303
pachyderm_branch_info_error = 304
pachyderm_field_name_error = 305
pachyderm_operation_error = 306
local_path_exception = 307

pachyderm_env_exception = 308
pachyderm_setup_error = 309
pachyderm_uninstallation_error = 310
helm_setup_failed = 311
minio_setup_failed = 312
helm_removal_failed = 313
minio_removal_failed = 314
pachyderm_connection_error = 315
pachyderm_server_error = 316
invalid_pachyderm_repo_info = 317
repo_permission_error = 318
# Email error codes
email_format_exception = 331
email_domain_exception = 332

# NFS
nfs_setup_failed = 340

# Visualization
heatmap_format_convert_exception = 401

# Exploration
invalid_attribute = 401
invalid_date = 402
input_length_mismatch = 403
dataset_import_failed = 405
data_size_exceeded = 409

# ExperimentException
experiment_failed = 1064
experiment_control_failed = 1065
experiment_field_error = 1066
run_field_error = 1071
run_failed = 1072

record_not_found = 404

# Service Mesh
service_mesh_creation_failed = 411
service_mesh_deployment_failed = 412

# HTTP Request Exception
http_request_failed = 501
invalid_http_request = 502

# Inference Service
inference_service_deployment_failed = 421

# abstract_pipeline_component
required_parameter_not_found = 404

# Data connections
connections_failed_to_connect = 701
invalid_key_value_pair = 702
invalid_path = 703
fs_client_creation_failed = 704
fs_client_list_dir_failed = 705
fs_client_unsupported_file_type = 706
invalid_method_call = 707
connection_to_database_failed = 708
connection_to_bigquery_failed = 709
query_job_to_bigquery_failed = 710

# file operation errors
file_exists_error = 801
